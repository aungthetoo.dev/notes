import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  notes: [],
};

const noteSlice = createSlice({
  name: "note",
  initialState,
  reducers: {
    createNote: (state, action) => {
      state.notes = [action.payload, ...state.notes];
    },
    updateNote: (state, action) => {},
  },
});

export const { reducer: noteReducer, actions: noteActions } = noteSlice;
