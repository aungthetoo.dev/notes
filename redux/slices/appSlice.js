import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: null,
};

const appSlice = createSlice({
  name: "app",
  initialState,
  reducers: {
    login: (state, action) => {
      state.user = action.payload;
    },
    logout: (state, action) => {
      state.user = null;
    },
  },
});

export const { reducer: appReducer, actions: appActions } = appSlice;
