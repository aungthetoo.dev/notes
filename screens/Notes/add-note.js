import React, { useEffect, useState } from "react";
import { View, Text, TextInput, Button } from "react-native";
import * as FileSystem from "expo-file-system";
import { encrypt } from "../../core/Helper";
import { styles } from "./styles";
import { useDispatch } from "react-redux";
import { noteActions } from "../../redux/slices/noteSlice";

const AddNote = ({ navigation }) => {
  const dispatch = useDispatch();
  const [note, setNote] = useState({
    title: "",
    content: "",
  });

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => <Button title="Save" onPress={saveFile} />,
    });
  }, []);

  const saveFile = async () => {
    const { title, content } = note;
    console.log(note);
    if (title.trim() == "" || content.trim() == "") {
      return alert("Title & Content can't be empty");
    }

    const encryptedData = encrypt(content);
    const fileName = `${title}_${Date.now()}`;
    try {
      const fileUri = `${FileSystem.documentDirectory}${fileName}`;
      await FileSystem.writeAsStringAsync(fileUri, encryptedData);
      dispatch(noteActions.createNote({ title, fileUri }));
      navigation.goBack();
    } catch (error) {
      alert("couldnot save file");
      console.error("Error saving encrypted file:", error);
    }
  };

  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Title"
        style={styles.textInput}
        autoCapitalize="none"
        value={note.title}
        onChangeText={(title) => {
          setNote({ ...note, title });
        }}
      />
      <TextInput
        placeholder="Content"
        multiline
        numberOfLines={10}
        style={styles.textarea}
        autoCapitalize="none"
        value={note.content}
        onChangeText={(content) => {
          setNote({ ...note, content });
        }}
      />
    </View>
  );
};

export default AddNote;
