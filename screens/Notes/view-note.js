import { View, Text, ActivityIndicator } from "react-native";
import React, { useEffect, useState } from "react";
import * as FileSystem from "expo-file-system";
import { styles } from "./styles";
import { decrypt } from "../../core/Helper";

const ViewNote = ({ route, navigation }) => {
  const { title, fileUri } = route.params;
  const [note, setNote] = useState({
    loading: true,
    data: "",
    error: null,
  });

  const decodeFileContent = async () => {
    try {
      setNote({
        ...note,
        loading: true,
      });
      const encryptedData = await FileSystem.readAsStringAsync(fileUri);
      const data = decrypt(encryptedData);
      setNote({
        loading: false,
        data,
        error: null,
      });
    } catch (error) {
      setNote({
        ...note,
        loading: false,
        error: error,
      });
      console.error("Error retrieving and decrypting file:", error);
    }
  };

  useEffect(() => {
    navigation.setOptions({
      title,
    });
    decodeFileContent();
  }, []);

  if (note.loading)
    return (
      <View style={styles.container}>
        <ActivityIndicator />
      </View>
    );

  return (
    <View style={styles.container}>
      <Text>{note.data}</Text>
    </View>
  );
};

export default ViewNote;
