import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  textInput: {
    width: "100%",
    borderWidth: 0.5,
    borderColor: "#cecece",
    padding: 16,
    borderRadius: 16,
    marginBottom: 16,
  },
  textarea: {
    width: "100%",
    borderWidth: 0.5,
    borderColor: "#cecece",
    padding: 16,
    borderRadius: 16,
    marginBottom: 16,
    minHeight: 200,
  },
});
