import { StyleSheet } from "react-native";
import { SCREEN_WIDTH } from "../../core/Helper";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 16,
  },
  title: {
    fontWeight: "bold",
    fontSize: 16,
    lineHeight: 32,
  },
  infoContainer: {
    width: "100%",
    alignItems: "center",
  },
  textInput: {
    borderWidth: 0.5,
    borderColor: "#cecece",
    width: "80%",
    padding: 12,
    borderRadius: 8,
    marginVertical: 8,
  },
});
