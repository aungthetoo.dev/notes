import { View, Text, TextInput, Button } from "react-native";
import React, { useEffect, useState } from "react";
import { styles } from "./styles";
import * as LocalAuthentication from "expo-local-authentication";
import { useDispatch } from "react-redux";
import { appActions } from "../../redux/slices/appSlice";

const Login = () => {
  const [local_available, setLocal_available] = useState(false);
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();

  const checkLocalAuthAvailable = async () => {
    try {
      const has_hardware = await LocalAuthentication.hasHardwareAsync();
      const is_enrolled = await LocalAuthentication.isEnrolledAsync();
      const supported_auth =
        await LocalAuthentication.supportedAuthenticationTypesAsync();

      const available_auth = supported_auth.every((auth) =>
        [
          LocalAuthentication.AuthenticationType.FACIAL_RECOGNITION,
          LocalAuthentication.AuthenticationType.FINGERPRINT,
        ].includes(auth)
      );

      setLocal_available(has_hardware && is_enrolled && available_auth);
    } catch (e) {
      setLocal_available(false);
    }
  };

  const handleLocalLogin = async () => {
    try {
      const { success } = await LocalAuthentication.authenticateAsync({
        disableDeviceFallback: true,
        cancelLabel: "Cancel",
      });
      if (success) {
        dispatch(
          appActions.login({
            name: "Guest",
            loginAt: Date.now(),
          })
        );
      } else {
        alert("face/finger not match");
      }
    } catch (e) {
      console.log(e);
    }
  };

  const handlePasswordLogin = async () => {
    if (password == "password") {
      dispatch(
        appActions.login({
          name: "Guest",
          loginAt: Date.now(),
        })
      );
    } else {
      alert("wrong password");
    }
  };

  useEffect(() => {
    checkLocalAuthAvailable();
  }, []);

  return (
    <View style={styles.container}>
      {local_available ? (
        <View style={styles.infoContainer}>
          <Text>Login with Local Auth</Text>
          <Button title="Use Face / Finger" onPress={handleLocalLogin} />
        </View>
      ) : (
        <View style={styles.infoContainer}>
          <Text style={styles.title}>Finger/Face ID not available</Text>
          <Text>Login with Password</Text>
          <TextInput
            value={password}
            onChangeText={setPassword}
            style={styles.textInput}
            placeholder="password"
            autoCapitalize="none"
          />
          <Button title="Continue" onPress={handlePasswordLogin} />
        </View>
      )}
    </View>
  );
};

export default Login;
