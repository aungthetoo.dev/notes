import { View, Text, Button, FlatList } from "react-native";
import React, { useEffect } from "react";
import { styles } from "./styles";
import { useSelector } from "react-redux";
import { NoteCard } from "../../components/NoteCard";

const Home = () => {
  const { notes } = useSelector((state) => state.notes);
  return (
    <FlatList
      style={styles.container}
      contentContainerStyle={{ padding: 8 }}
      showsVerticalScrollIndicator={false}
      data={notes}
      renderItem={({ item, index }) => <NoteCard key={index} {...item} />}
    />
  );
};

export default Home;
