import { Dimensions } from "react-native";
import CryptoJS from "crypto-js";

const secretKey = "very_long_secure_key"; //temp

export const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } =
  Dimensions.get("screen");

export const encrypt = (content) => {
  return CryptoJS.AES.encrypt(content, secretKey).toString();
};

export const decrypt = (encryptedContent) => {
  const bytes = CryptoJS.AES.decrypt(encryptedContent, secretKey);
  return bytes.toString(CryptoJS.enc.Utf8);
};
