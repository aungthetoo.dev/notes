import { NavigationContainer, useNavigation } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "../screens/Home";
import Login from "../screens/Login";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "react-native";
import { appActions } from "../redux/slices/appSlice";
import AddNote from "../screens/Notes/add-note";
import ViewNote from "../screens/Notes/view-note";

const Stack = createNativeStackNavigator();

const RootStackNavigation = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="notes"
        component={Home}
        options={{
          headerLeft: () => (
            <Button
              title="Add New"
              onPress={() => navigation.navigate("add-notes")}
            />
          ),
          headerRight: () => (
            <Button
              title="Logout"
              onPress={() => dispatch(appActions.logout())}
            />
          ),
          title: "Notes",
        }}
      />
      <Stack.Screen
        name="add-notes"
        component={AddNote}
        options={{
          title: "Add New Note",
        }}
      />
      <Stack.Screen name="view-notes" component={ViewNote} />
    </Stack.Navigator>
  );
};

const AuthStackNavigation = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="login"
        component={Login}
        options={{
          title: "Login",
        }}
      />
    </Stack.Navigator>
  );
};

const Navigation = () => {
  const { user } = useSelector((state) => state.app);
  return (
    <NavigationContainer>
      {user ? <RootStackNavigation /> : <AuthStackNavigation />}
    </NavigationContainer>
  );
};

export default Navigation;
