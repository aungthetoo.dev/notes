import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import React from "react";
import { useNavigation } from "@react-navigation/native";

export const NoteCard = ({ title, fileUri }) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.container}
      activeOpacity={0.5}
      onPress={() => navigation.navigate("view-notes", { title, fileUri })}
    >
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 50,
    borderWidth: 0.5,
    borderColor: "#cecece",
    borderRadius: 8,
    marginBottom: 16,
    justifyContent: "center",
    padding: 8,
  },
  title: {
    fontWeight: "bold",
    fontSize: 16,
  },
});
